// Communication
#define NB_MOT 10
String inputString[NB_MOT+1];
bool stringComplete = false;
int numero_string = 0;

#include "omni-2022/main.cpp"
#include "actionneur_walter/main.cpp"

void setup() {
    Serial.begin(115200);
    setup_omni_2022();
}


void loop() {
  loop_omni_2022();
  delay(2);  //défini la période entre chaque calcul
}

// interruption de la réception d'une trame
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }else{
      if(inChar == ';'){
        numero_string+=1;
      }else{
        inputString[numero_string] += inChar;
      }
    }
  }
}
